public class FindPrimes {

        public static void main (String[] args) {


            //Get the number
            int given = Integer.parseInt(args[0]);


            //For each number less than the given
             for (int number = 2; number< given; number ++) {

                 boolean isPrime = true;


                 // for each number less than number

                 for (int divisor =2; divisor < number; divisor++)
                    //if number is divisible by divisor
                     if (number % divisor ==0){
                         //is prime is false
                         isPrime = false;
                         //end the loop
                         break;
                     }




                 //If the number is prime
                 if (isPrime)

                 //print the number
                     System.out.println(number +",");
             }
        }

}


